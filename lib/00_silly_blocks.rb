# # Topics
#
# * blocks
# * closures
# * yield
# * loops

def reverser
  yield.split.each do |word|
    word.reverse!
  end.join(" ")
end 

def adder(num=1)
  yield+num
end

def repeater(n=1)
  n.times do
    yield
  end
end

#
#   describe "repeater" do
#     it "executes the default block" do
#       block_was_executed = false
#
#       repeater { block_was_executed = true }
#
#       expect(block_was_executed).to be_truthy
#     end
#
#     it "executes the default block 3 times" do
#       n = 0
#
#       repeater(3) { n += 1 }
#
#       expect(n).to eq(3)
#     end
#
#     it "executes the default block 10 times" do
#       n = 0
#
#       repeater(10) { n += 1 }
#
#       expect(n).to eq(10)
#     end
#   end
# end

def measure(n=1)
  total = 0
  n.times do
    start_time = Time.now
    yield
  
    total += Time.now - start_time
  end
  total/n.to_f
end
